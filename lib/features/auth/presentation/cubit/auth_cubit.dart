import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/enums.dart';
import 'package:flutter_task/core/error/failures.dart';
import 'package:flutter_task/features/auth/domain/repositories/auth_repo.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../data/models/user.dart';

part 'auth_state.dart';
part 'auth_cubit.freezed.dart';

@injectable
class AuthCubit extends Cubit<AuthState> {
  final AuthRepo _authRepo;
  AuthCubit(this._authRepo) : super(const AuthState.loading());

  Future<void> checkAuthentication() async {
    final either = await _authRepo.checkAuthentication();
    either.fold(
        (failure) => emit(const AuthState.unauthenticated()),
        (user) => user.type == UserType.student
            ? emit(const AuthState.authenticatedAsStudent())
            : emit(const AuthState.authenticatedAsTeacher()));
  }

  Future<void> login(String email, String password) async {
    emit(const AuthState.loading());

    var either = await _authRepo.login(email, password);

    either.fold(
        (failure) => emit(AuthState.error(failure: failure)),
        (user) => user.type == UserType.student
            ? emit(const AuthState.authenticatedAsStudent())
            : emit(const AuthState.authenticatedAsTeacher()));
  }

  Future<void> register(User user) async {
    emit(const AuthState.loading());
    final either = await _authRepo.register(user);

    either.fold(
        (failure) => emit(AuthState.error(failure: failure)),
        (user) => user.type == UserType.student
            ? emit(const AuthState.authenticatedAsStudent())
            : emit(const AuthState.authenticatedAsTeacher()));
  }

  Future<void> logout() async {
    emit(const AuthState.loading());
    await _authRepo.logout();
    emit(const AuthState.unauthenticated());
  }

  @override
  void onChange(Change<AuthState> change) {
    print(change);
    super.onChange(change);
  }
}
