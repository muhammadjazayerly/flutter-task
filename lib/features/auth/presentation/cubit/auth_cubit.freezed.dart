// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;
}

/// @nodoc
abstract class _$$_AuthStateUnauthenticatedCopyWith<$Res> {
  factory _$$_AuthStateUnauthenticatedCopyWith(
          _$_AuthStateUnauthenticated value,
          $Res Function(_$_AuthStateUnauthenticated) then) =
      __$$_AuthStateUnauthenticatedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AuthStateUnauthenticatedCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$$_AuthStateUnauthenticatedCopyWith<$Res> {
  __$$_AuthStateUnauthenticatedCopyWithImpl(_$_AuthStateUnauthenticated _value,
      $Res Function(_$_AuthStateUnauthenticated) _then)
      : super(_value, (v) => _then(v as _$_AuthStateUnauthenticated));

  @override
  _$_AuthStateUnauthenticated get _value =>
      super._value as _$_AuthStateUnauthenticated;
}

/// @nodoc

class _$_AuthStateUnauthenticated implements _AuthStateUnauthenticated {
  const _$_AuthStateUnauthenticated();

  @override
  String toString() {
    return 'AuthState.unauthenticated()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthStateUnauthenticated);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) {
    return unauthenticated();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) {
    return unauthenticated?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) {
    return unauthenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) {
    return unauthenticated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(this);
    }
    return orElse();
  }
}

abstract class _AuthStateUnauthenticated implements AuthState {
  const factory _AuthStateUnauthenticated() = _$_AuthStateUnauthenticated;
}

/// @nodoc
abstract class _$$_AuthStateLoadingCopyWith<$Res> {
  factory _$$_AuthStateLoadingCopyWith(
          _$_AuthStateLoading value, $Res Function(_$_AuthStateLoading) then) =
      __$$_AuthStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AuthStateLoadingCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$$_AuthStateLoadingCopyWith<$Res> {
  __$$_AuthStateLoadingCopyWithImpl(
      _$_AuthStateLoading _value, $Res Function(_$_AuthStateLoading) _then)
      : super(_value, (v) => _then(v as _$_AuthStateLoading));

  @override
  _$_AuthStateLoading get _value => super._value as _$_AuthStateLoading;
}

/// @nodoc

class _$_AuthStateLoading implements _AuthStateLoading {
  const _$_AuthStateLoading();

  @override
  String toString() {
    return 'AuthState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_AuthStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _AuthStateLoading implements AuthState {
  const factory _AuthStateLoading() = _$_AuthStateLoading;
}

/// @nodoc
abstract class _$$_AuthStateAuthenticatedAsTeacherCopyWith<$Res> {
  factory _$$_AuthStateAuthenticatedAsTeacherCopyWith(
          _$_AuthStateAuthenticatedAsTeacher value,
          $Res Function(_$_AuthStateAuthenticatedAsTeacher) then) =
      __$$_AuthStateAuthenticatedAsTeacherCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AuthStateAuthenticatedAsTeacherCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$$_AuthStateAuthenticatedAsTeacherCopyWith<$Res> {
  __$$_AuthStateAuthenticatedAsTeacherCopyWithImpl(
      _$_AuthStateAuthenticatedAsTeacher _value,
      $Res Function(_$_AuthStateAuthenticatedAsTeacher) _then)
      : super(_value, (v) => _then(v as _$_AuthStateAuthenticatedAsTeacher));

  @override
  _$_AuthStateAuthenticatedAsTeacher get _value =>
      super._value as _$_AuthStateAuthenticatedAsTeacher;
}

/// @nodoc

class _$_AuthStateAuthenticatedAsTeacher
    implements _AuthStateAuthenticatedAsTeacher {
  const _$_AuthStateAuthenticatedAsTeacher();

  @override
  String toString() {
    return 'AuthState.authenticatedAsTeacher()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthStateAuthenticatedAsTeacher);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) {
    return authenticatedAsTeacher();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) {
    return authenticatedAsTeacher?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (authenticatedAsTeacher != null) {
      return authenticatedAsTeacher();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) {
    return authenticatedAsTeacher(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) {
    return authenticatedAsTeacher?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) {
    if (authenticatedAsTeacher != null) {
      return authenticatedAsTeacher(this);
    }
    return orElse();
  }
}

abstract class _AuthStateAuthenticatedAsTeacher implements AuthState {
  const factory _AuthStateAuthenticatedAsTeacher() =
      _$_AuthStateAuthenticatedAsTeacher;
}

/// @nodoc
abstract class _$$_AuthStateAuthenticatedAsStudentCopyWith<$Res> {
  factory _$$_AuthStateAuthenticatedAsStudentCopyWith(
          _$_AuthStateAuthenticatedAsStudent value,
          $Res Function(_$_AuthStateAuthenticatedAsStudent) then) =
      __$$_AuthStateAuthenticatedAsStudentCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AuthStateAuthenticatedAsStudentCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$$_AuthStateAuthenticatedAsStudentCopyWith<$Res> {
  __$$_AuthStateAuthenticatedAsStudentCopyWithImpl(
      _$_AuthStateAuthenticatedAsStudent _value,
      $Res Function(_$_AuthStateAuthenticatedAsStudent) _then)
      : super(_value, (v) => _then(v as _$_AuthStateAuthenticatedAsStudent));

  @override
  _$_AuthStateAuthenticatedAsStudent get _value =>
      super._value as _$_AuthStateAuthenticatedAsStudent;
}

/// @nodoc

class _$_AuthStateAuthenticatedAsStudent
    implements _AuthStateAuthenticatedAsStudent {
  const _$_AuthStateAuthenticatedAsStudent();

  @override
  String toString() {
    return 'AuthState.authenticatedAsStudent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthStateAuthenticatedAsStudent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) {
    return authenticatedAsStudent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) {
    return authenticatedAsStudent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (authenticatedAsStudent != null) {
      return authenticatedAsStudent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) {
    return authenticatedAsStudent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) {
    return authenticatedAsStudent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) {
    if (authenticatedAsStudent != null) {
      return authenticatedAsStudent(this);
    }
    return orElse();
  }
}

abstract class _AuthStateAuthenticatedAsStudent implements AuthState {
  const factory _AuthStateAuthenticatedAsStudent() =
      _$_AuthStateAuthenticatedAsStudent;
}

/// @nodoc
abstract class _$$_AuthStateErrorCopyWith<$Res> {
  factory _$$_AuthStateErrorCopyWith(
          _$_AuthStateError value, $Res Function(_$_AuthStateError) then) =
      __$$_AuthStateErrorCopyWithImpl<$Res>;
  $Res call({Failure? failure});
}

/// @nodoc
class __$$_AuthStateErrorCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$$_AuthStateErrorCopyWith<$Res> {
  __$$_AuthStateErrorCopyWithImpl(
      _$_AuthStateError _value, $Res Function(_$_AuthStateError) _then)
      : super(_value, (v) => _then(v as _$_AuthStateError));

  @override
  _$_AuthStateError get _value => super._value as _$_AuthStateError;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_$_AuthStateError(
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure?,
    ));
  }
}

/// @nodoc

class _$_AuthStateError implements _AuthStateError {
  const _$_AuthStateError({this.failure});

  @override
  final Failure? failure;

  @override
  String toString() {
    return 'AuthState.error(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthStateError &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(failure));

  @JsonKey(ignore: true)
  @override
  _$$_AuthStateErrorCopyWith<_$_AuthStateError> get copyWith =>
      __$$_AuthStateErrorCopyWithImpl<_$_AuthStateError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unauthenticated,
    required TResult Function() loading,
    required TResult Function() authenticatedAsTeacher,
    required TResult Function() authenticatedAsStudent,
    required TResult Function(Failure? failure) error,
  }) {
    return error(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
  }) {
    return error?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unauthenticated,
    TResult Function()? loading,
    TResult Function()? authenticatedAsTeacher,
    TResult Function()? authenticatedAsStudent,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthStateUnauthenticated value) unauthenticated,
    required TResult Function(_AuthStateLoading value) loading,
    required TResult Function(_AuthStateAuthenticatedAsTeacher value)
        authenticatedAsTeacher,
    required TResult Function(_AuthStateAuthenticatedAsStudent value)
        authenticatedAsStudent,
    required TResult Function(_AuthStateError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthStateUnauthenticated value)? unauthenticated,
    TResult Function(_AuthStateLoading value)? loading,
    TResult Function(_AuthStateAuthenticatedAsTeacher value)?
        authenticatedAsTeacher,
    TResult Function(_AuthStateAuthenticatedAsStudent value)?
        authenticatedAsStudent,
    TResult Function(_AuthStateError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _AuthStateError implements AuthState {
  const factory _AuthStateError({final Failure? failure}) = _$_AuthStateError;

  Failure? get failure;
  @JsonKey(ignore: true)
  _$$_AuthStateErrorCopyWith<_$_AuthStateError> get copyWith =>
      throw _privateConstructorUsedError;
}
