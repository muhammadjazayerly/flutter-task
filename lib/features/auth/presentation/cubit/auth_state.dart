part of 'auth_cubit.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState.unauthenticated() = _AuthStateUnauthenticated;
  const factory AuthState.loading() = _AuthStateLoading;
  const factory AuthState.authenticatedAsTeacher() =
      _AuthStateAuthenticatedAsTeacher;
  const factory AuthState.authenticatedAsStudent() =
      _AuthStateAuthenticatedAsStudent;
  const factory AuthState.error({Failure? failure}) = _AuthStateError;
}
