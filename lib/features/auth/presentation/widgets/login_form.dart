import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/widgets/custom_button.dart';
import 'package:flutter_task/core/widgets/custom_text_field.dart';
import 'package:flutter_task/features/auth/presentation/cubit/auth_cubit.dart';

import '../../../../core/functions.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextField(
            hint: 'Email',
            controller: _emailController,
            icon: Icons.email,
            validator: emailValidator,
            textInputType: TextInputType.emailAddress,
          ),
          const SizedBox(height: 20),
          CustomTextField(
            hint: 'Password',
            controller: _passwordController,
            icon: Icons.lock,
            validator: passwordValidator,
            obscureText: true,
            toggleShowHideText: true,
          ),
          const SizedBox(height: 40),
          BlocBuilder<AuthCubit, AuthState>(
            builder: (context, state) {
              return state.maybeWhen(
                loading: () => const Center(
                  child: CircularProgressIndicator.adaptive(),
                ),
                orElse: () => CustomButton(
                  onClick: () {
                    if (_formKey.currentState!.validate()) {
                      context.read<AuthCubit>().login(
                          _emailController.text, _passwordController.text);
                    }
                  },
                  text: 'LOGIN',
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
