import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/enums.dart';
import 'package:uuid/uuid.dart';

import '../../../../core/functions.dart';
import '../../../../core/widgets/custom_button.dart';
import '../../../../core/widgets/custom_text_field.dart';
import '../../data/models/user.dart';
import '../cubit/auth_cubit.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();

  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();

  final _confirmPasswordController = TextEditingController();

  bool _valid = true;

  UserType? _userType;

  String? _confirmPasswordValidator(String? val) {
    if (_confirmPasswordController.text.isEmpty) {
      return 'Required field';
    } else if (_passwordController.text != _confirmPasswordController.text) {
      return 'Wrong, not matched';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextField(
            hint: 'FullName',
            controller: _nameController,
            icon: Icons.person,
            validator: nameValidator,
          ),
          const SizedBox(height: 20),
          CustomTextField(
            hint: 'Email',
            controller: _emailController,
            icon: Icons.email,
            validator: emailValidator,
            textInputType: TextInputType.emailAddress,
          ),
          const SizedBox(height: 20),
          CustomTextField(
            hint: 'Password',
            controller: _passwordController,
            icon: Icons.lock,
            obscureText: true,
            toggleShowHideText: true,
            validator: passwordValidator,
          ),
          const SizedBox(height: 20),
          CustomTextField(
            hint: 'Confirm Password',
            controller: _confirmPasswordController,
            icon: Icons.lock,
            obscureText: true,
            toggleShowHideText: true,
            validator: _confirmPasswordValidator,
          ),
          const SizedBox(height: 20),
          DropdownButton<UserType>(
            items: [
              DropdownMenuItem(
                value: UserType.teacher,
                child: Text(
                  UserType.teacher.name,
                ),
              ),
              DropdownMenuItem(
                value: UserType.student,
                child: Text(
                  UserType.student.name,
                ),
              ),
            ],
            hint: const Text('Select Your Role'),
            value: _userType,
            onChanged: (value) {
              setState(() {
                _userType = value;
                _valid = true;
              });
            },
          ),
          const SizedBox(height: 20),
          Visibility(
            visible: !_valid,
            child: Text(
              'Please Complete the Form',
              style: TextStyle(color: Colors.red.shade600),
            ),
          ),
          const SizedBox(height: 30),
          BlocBuilder<AuthCubit, AuthState>(
            builder: (context, state) {
              return state.maybeWhen(
                  loading: () =>
                      const Center(child: CircularProgressIndicator.adaptive()),
                  orElse: () => CustomButton(
                        onClick: () {
                          if (_formKey.currentState!.validate() &&
                              _userType != null) {
                            context.read<AuthCubit>().register(
                                  User(
                                    id: const Uuid().v1(),
                                    name: _nameController.text,
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                    type: _userType!,
                                  ),
                                );
                          } else if (_userType == null) {
                            setState(() {
                              _valid = false;
                            });
                          }
                        },
                        text: 'REGISTER',
                      ));
            },
          ),
        ],
      ),
    );
  }
}
