import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/features/auth/presentation/cubit/auth_cubit.dart';
import 'package:flutter_task/features/auth/presentation/widgets/register_form.dart';

import '../../../../core/config/app_router.gr.dart';
import '../../../../core/error/failures.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          state.maybeWhen(
              error: (failure) {
                if (failure is EmailAlreadyExsitsFailure) {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Sorry, Email Already exsits')));
                } else if (failure is FirebaseFailure) {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text(failure.message!)),
                  );
                } else if (failure is NetworkFailure) {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content:
                          Text('no internet, please check your connection'),
                      behavior: SnackBarBehavior.floating,
                    ),
                  );
                }
              },
              orElse: () {});
        },
        child: Scaffold(
          body: Center(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  const SizedBox(height: 30),
                  Text(
                    'Register',
                    style: Theme.of(context).textTheme.displaySmall,
                  ),
                  const SizedBox(height: 30),
                  const RegisterForm(),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
