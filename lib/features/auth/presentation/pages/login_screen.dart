import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/config/app_router.gr.dart';
import 'package:flutter_task/core/error/failures.dart';
import 'package:flutter_task/features/auth/presentation/cubit/auth_cubit.dart';
import 'package:flutter_task/features/auth/presentation/widgets/login_form.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: context.read<AuthCubit>(),
      child: Builder(builder: (context) {
        return BlocListener<AuthCubit, AuthState>(
          listener: (context, state) {
            state.maybeWhen(
                authenticatedAsTeacher: () {
                  context.router.pushAndPopUntil(
                    const MyAnnouncementsScreen(),
                    predicate: (route) => false,
                  );
                },
                authenticatedAsStudent: () {
                  context.router.pushAndPopUntil(
                    const AnnouncementsScreen(),
                    predicate: (route) => false,
                  );
                },
                error: (failure) {
                  if (failure is EmailOrPasswordIsWrongFailure) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Sorry, Email or Password is Wrong')));
                  } else if (failure is FirebaseFailure) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(failure.message!)),
                    );
                  } else if (failure is NetworkFailure) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content:
                            Text('no internet, please check your connection'),
                        behavior: SnackBarBehavior.floating,
                      ),
                    );
                  }
                },
                orElse: () {});
          },
          child: Scaffold(
            body: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      'Welcome',
                      style: Theme.of(context).textTheme.displaySmall,
                    ),
                    const SizedBox(height: 40),
                    const LoginForm(),
                    const SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("dont't have an account?"),
                        TextButton(
                          onPressed: () {
                            context.router.push(const RegisterScreen());
                          },
                          child: const Text('Register'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }
}
