import 'package:flutter_task/core/error/failures.dart';
import 'package:fpdart/fpdart.dart';

import '../../data/models/user.dart';

abstract class AuthRepo {
  Future<Either<Failure, User>> login(String email, String password);
  Future<Either<Failure, User>> register(User user);
  Future<Either<Failure, User>> checkAuthentication();
  Future<void> logout();
}
