import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_task/core/error/exceptions.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/enums.dart';
import '../models/user.dart';

abstract class AuthRemoteDS {
  Future<User> login(String email, String password);
  Future<User> register(User user);
  Future<void> logout();
  Future<User?> checkAuthentication();
}

const String USER_KEY = 'USER';

@LazySingleton(as: AuthRemoteDS)
class AuthRemoteDSImpl implements AuthRemoteDS {
  final SharedPreferences _preferences;
  final FirebaseFirestore _firestore;

  AuthRemoteDSImpl(this._preferences, this._firestore);

  @override
  Future<User> login(String email, String password) async {
    var result = await checkIfEmailExists(email);
    if (result != null) {
      var data = result.docs.first.data();
      if (data['password'] == password) {
        var user = User.fromMap(data);
        cacheUser(user);
        return user;
      }
    }
    throw EmailOrPasswordIsWrongException();
  }

  @override
  Future<User> register(User user) async {
    var result = await checkIfEmailExists(user.email);
    if (result == null) {
      await _firestore
          .collection('${user.type.name}s')
          .doc(user.id)
          .set(user.toMap());
      cacheUser(user);
      return user;
    } else {
      throw EmailAlreadyExsitsException();
    }
  }

  Future<void> cacheUser(User user) async {
    await _preferences.setString(USER_KEY, user.toJson());
  }

  Future<QuerySnapshot<Map<String, dynamic>>?> checkIfEmailExists(
      String email) async {
    /// First Search in teachers collection
    /// then in students collection
    var res1 = await _firestore
        .collection('${UserType.student.name}s')
        .where('email', isEqualTo: email)
        .limit(1)
        .get();
    if (res1.docs.isNotEmpty) return res1;

    var res2 = await _firestore
        .collection('${UserType.teacher.name}s')
        .where('email', isEqualTo: email)
        .limit(1)
        .get();
    if (res2.docs.isNotEmpty) return res2;

    return null;
  }

  @override
  Future<User?> checkAuthentication() async {
    var jsonObj = _preferences.get(USER_KEY);
    if (jsonObj != null) {
      var user = User.fromJson(jsonObj as String);
      return user;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> logout() async {
    await _preferences.remove(USER_KEY);
  }
}
