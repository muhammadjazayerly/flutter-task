import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_task/core/error/exceptions.dart';
import 'package:flutter_task/core/request.dart';
import 'package:flutter_task/features/auth/data/datasources/auth_remote_ds.dart';
import 'package:flutter_task/features/auth/data/models/user.dart';
import 'package:flutter_task/core/error/failures.dart';
import 'package:flutter_task/features/auth/domain/repositories/auth_repo.dart';
import 'package:fpdart/fpdart.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: AuthRepo)
class AuthRepoImpl implements AuthRepo {
  final AuthRemoteDS _remoteDS;

  AuthRepoImpl(this._remoteDS);
  @override
  Future<Either<Failure, User>> login(String email, String password) async {
    try {
      return await sendRequest<Either<Failure, User>>(() async {
        var user = await _remoteDS.login(email, password);
        return Right<Failure, User>(user);
      });
    } on EmailOrPasswordIsWrongException {
      return Left<Failure, User>(EmailOrPasswordIsWrongFailure());
    } on NetworkException {
      return Left<Failure, User>(NetworkFailure());
    } on FirebaseException catch (e) {
      return Left<Failure, User>(FirebaseFailure(message: e.message));
    }
  }

  @override
  Future<Either<Failure, User>> register(User user) async {
    try {
      return await sendRequest<Either<Failure, User>>(() async {
        await _remoteDS.register(user);
        return Right<Failure, User>(user);
      });
    } on EmailAlreadyExsitsException {
      return Left<Failure, User>(EmailAlreadyExsitsFailure());
    } on NetworkException {
      return Left<Failure, User>(NetworkFailure());
    } on FirebaseException catch (e) {
      return Left<Failure, User>(FirebaseFailure(message: e.message));
    }
  }

  @override
  Future<Either<Failure, User>> checkAuthentication() async {
    try {
      var user = await _remoteDS.checkAuthentication();
      return Right<Failure, User>(user!);
    } on CacheException {
      return Left<Failure, User>(CacheFailure());
    }
  }

  @override
  Future<void> logout() async {
    await _remoteDS.logout();
  }
}
