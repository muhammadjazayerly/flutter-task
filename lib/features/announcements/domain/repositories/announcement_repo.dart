import 'package:flutter_task/core/error/failures.dart';
import 'package:fpdart/fpdart.dart';

import '../../data/models/announcement.dart';

abstract class AnnouncementRepo {
  Future<Either<Failure, void>> publishAnnouncement(Announcement announcement);
  Future<Either<Failure, List<Announcement>>> getAnnouncements();
  Future<Either<Failure, List<Announcement>>> getAnnouncementsById();
}
