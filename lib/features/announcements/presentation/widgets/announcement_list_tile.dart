import 'package:flutter/material.dart';
import 'package:flutter_task/features/announcements/data/models/announcement.dart';

class AnnouncementListTile extends StatelessWidget {
  final Announcement announcement;
  final bool showPublisher;
  const AnnouncementListTile(
      {Key? key, required this.announcement, this.showPublisher = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      margin: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        color: Colors.indigo.shade50,
        borderRadius: BorderRadius.circular(12.0),
        boxShadow: const [
          BoxShadow(
            blurRadius: 1,
            color: Colors.grey,
          ),
        ],
      ),
      child: Column(
        children: [
          Text(
            announcement.title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
            ),
          ),
          const SizedBox(height: 8),
          const Divider(),
          const SizedBox(height: 10),
          Text(
            announcement.description,
            textAlign: TextAlign.start,
          ),
          const SizedBox(height: 10),
          Visibility(visible: showPublisher, child: const Divider()),
          Visibility(
            visible: showPublisher,
            child: Text('Published by: ${announcement.publisherName}'),
          )
        ],
      ),
    );
  }
}
