import 'package:flutter/material.dart';

import '../../data/models/announcement.dart';
import 'announcement_list_tile.dart';

class AnnouncementsListWidget extends StatelessWidget {
  final bool showPublisher;

  final List<Announcement> announcements;
  const AnnouncementsListWidget({
    Key? key,
    required this.announcements,
    this.showPublisher = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.symmetric(vertical: 10),
      itemBuilder: (context, index) => AnnouncementListTile(
        announcement: announcements[index],
        showPublisher: showPublisher,
      ),
      itemCount: announcements.length,
    );
  }
}
