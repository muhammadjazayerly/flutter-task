// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'announcements_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AnnouncementsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Announcement> announcements) loaded,
    required TResult Function(Failure? failure) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AnnouncementsStateInitial value) initial,
    required TResult Function(_AnnouncementsStateLoading value) loading,
    required TResult Function(_AnnouncementsStateLoaded value) loaded,
    required TResult Function(_AnnouncementsStateError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnnouncementsStateCopyWith<$Res> {
  factory $AnnouncementsStateCopyWith(
          AnnouncementsState value, $Res Function(AnnouncementsState) then) =
      _$AnnouncementsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AnnouncementsStateCopyWithImpl<$Res>
    implements $AnnouncementsStateCopyWith<$Res> {
  _$AnnouncementsStateCopyWithImpl(this._value, this._then);

  final AnnouncementsState _value;
  // ignore: unused_field
  final $Res Function(AnnouncementsState) _then;
}

/// @nodoc
abstract class _$$_AnnouncementsStateInitialCopyWith<$Res> {
  factory _$$_AnnouncementsStateInitialCopyWith(
          _$_AnnouncementsStateInitial value,
          $Res Function(_$_AnnouncementsStateInitial) then) =
      __$$_AnnouncementsStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AnnouncementsStateInitialCopyWithImpl<$Res>
    extends _$AnnouncementsStateCopyWithImpl<$Res>
    implements _$$_AnnouncementsStateInitialCopyWith<$Res> {
  __$$_AnnouncementsStateInitialCopyWithImpl(
      _$_AnnouncementsStateInitial _value,
      $Res Function(_$_AnnouncementsStateInitial) _then)
      : super(_value, (v) => _then(v as _$_AnnouncementsStateInitial));

  @override
  _$_AnnouncementsStateInitial get _value =>
      super._value as _$_AnnouncementsStateInitial;
}

/// @nodoc

class _$_AnnouncementsStateInitial implements _AnnouncementsStateInitial {
  const _$_AnnouncementsStateInitial();

  @override
  String toString() {
    return 'AnnouncementsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AnnouncementsStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Announcement> announcements) loaded,
    required TResult Function(Failure? failure) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AnnouncementsStateInitial value) initial,
    required TResult Function(_AnnouncementsStateLoading value) loading,
    required TResult Function(_AnnouncementsStateLoaded value) loaded,
    required TResult Function(_AnnouncementsStateError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _AnnouncementsStateInitial implements AnnouncementsState {
  const factory _AnnouncementsStateInitial() = _$_AnnouncementsStateInitial;
}

/// @nodoc
abstract class _$$_AnnouncementsStateLoadingCopyWith<$Res> {
  factory _$$_AnnouncementsStateLoadingCopyWith(
          _$_AnnouncementsStateLoading value,
          $Res Function(_$_AnnouncementsStateLoading) then) =
      __$$_AnnouncementsStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AnnouncementsStateLoadingCopyWithImpl<$Res>
    extends _$AnnouncementsStateCopyWithImpl<$Res>
    implements _$$_AnnouncementsStateLoadingCopyWith<$Res> {
  __$$_AnnouncementsStateLoadingCopyWithImpl(
      _$_AnnouncementsStateLoading _value,
      $Res Function(_$_AnnouncementsStateLoading) _then)
      : super(_value, (v) => _then(v as _$_AnnouncementsStateLoading));

  @override
  _$_AnnouncementsStateLoading get _value =>
      super._value as _$_AnnouncementsStateLoading;
}

/// @nodoc

class _$_AnnouncementsStateLoading implements _AnnouncementsStateLoading {
  const _$_AnnouncementsStateLoading();

  @override
  String toString() {
    return 'AnnouncementsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AnnouncementsStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Announcement> announcements) loaded,
    required TResult Function(Failure? failure) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AnnouncementsStateInitial value) initial,
    required TResult Function(_AnnouncementsStateLoading value) loading,
    required TResult Function(_AnnouncementsStateLoaded value) loaded,
    required TResult Function(_AnnouncementsStateError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _AnnouncementsStateLoading implements AnnouncementsState {
  const factory _AnnouncementsStateLoading() = _$_AnnouncementsStateLoading;
}

/// @nodoc
abstract class _$$_AnnouncementsStateLoadedCopyWith<$Res> {
  factory _$$_AnnouncementsStateLoadedCopyWith(
          _$_AnnouncementsStateLoaded value,
          $Res Function(_$_AnnouncementsStateLoaded) then) =
      __$$_AnnouncementsStateLoadedCopyWithImpl<$Res>;
  $Res call({List<Announcement> announcements});
}

/// @nodoc
class __$$_AnnouncementsStateLoadedCopyWithImpl<$Res>
    extends _$AnnouncementsStateCopyWithImpl<$Res>
    implements _$$_AnnouncementsStateLoadedCopyWith<$Res> {
  __$$_AnnouncementsStateLoadedCopyWithImpl(_$_AnnouncementsStateLoaded _value,
      $Res Function(_$_AnnouncementsStateLoaded) _then)
      : super(_value, (v) => _then(v as _$_AnnouncementsStateLoaded));

  @override
  _$_AnnouncementsStateLoaded get _value =>
      super._value as _$_AnnouncementsStateLoaded;

  @override
  $Res call({
    Object? announcements = freezed,
  }) {
    return _then(_$_AnnouncementsStateLoaded(
      announcements: announcements == freezed
          ? _value._announcements
          : announcements // ignore: cast_nullable_to_non_nullable
              as List<Announcement>,
    ));
  }
}

/// @nodoc

class _$_AnnouncementsStateLoaded implements _AnnouncementsStateLoaded {
  const _$_AnnouncementsStateLoaded(
      {required final List<Announcement> announcements})
      : _announcements = announcements;

  final List<Announcement> _announcements;
  @override
  List<Announcement> get announcements {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_announcements);
  }

  @override
  String toString() {
    return 'AnnouncementsState.loaded(announcements: $announcements)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AnnouncementsStateLoaded &&
            const DeepCollectionEquality()
                .equals(other._announcements, _announcements));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_announcements));

  @JsonKey(ignore: true)
  @override
  _$$_AnnouncementsStateLoadedCopyWith<_$_AnnouncementsStateLoaded>
      get copyWith => __$$_AnnouncementsStateLoadedCopyWithImpl<
          _$_AnnouncementsStateLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Announcement> announcements) loaded,
    required TResult Function(Failure? failure) error,
  }) {
    return loaded(announcements);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
  }) {
    return loaded?.call(announcements);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(announcements);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AnnouncementsStateInitial value) initial,
    required TResult Function(_AnnouncementsStateLoading value) loading,
    required TResult Function(_AnnouncementsStateLoaded value) loaded,
    required TResult Function(_AnnouncementsStateError value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _AnnouncementsStateLoaded implements AnnouncementsState {
  const factory _AnnouncementsStateLoaded(
          {required final List<Announcement> announcements}) =
      _$_AnnouncementsStateLoaded;

  List<Announcement> get announcements;
  @JsonKey(ignore: true)
  _$$_AnnouncementsStateLoadedCopyWith<_$_AnnouncementsStateLoaded>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_AnnouncementsStateErrorCopyWith<$Res> {
  factory _$$_AnnouncementsStateErrorCopyWith(_$_AnnouncementsStateError value,
          $Res Function(_$_AnnouncementsStateError) then) =
      __$$_AnnouncementsStateErrorCopyWithImpl<$Res>;
  $Res call({Failure? failure});
}

/// @nodoc
class __$$_AnnouncementsStateErrorCopyWithImpl<$Res>
    extends _$AnnouncementsStateCopyWithImpl<$Res>
    implements _$$_AnnouncementsStateErrorCopyWith<$Res> {
  __$$_AnnouncementsStateErrorCopyWithImpl(_$_AnnouncementsStateError _value,
      $Res Function(_$_AnnouncementsStateError) _then)
      : super(_value, (v) => _then(v as _$_AnnouncementsStateError));

  @override
  _$_AnnouncementsStateError get _value =>
      super._value as _$_AnnouncementsStateError;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_$_AnnouncementsStateError(
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure?,
    ));
  }
}

/// @nodoc

class _$_AnnouncementsStateError implements _AnnouncementsStateError {
  const _$_AnnouncementsStateError({this.failure});

  @override
  final Failure? failure;

  @override
  String toString() {
    return 'AnnouncementsState.error(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AnnouncementsStateError &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(failure));

  @JsonKey(ignore: true)
  @override
  _$$_AnnouncementsStateErrorCopyWith<_$_AnnouncementsStateError>
      get copyWith =>
          __$$_AnnouncementsStateErrorCopyWithImpl<_$_AnnouncementsStateError>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Announcement> announcements) loaded,
    required TResult Function(Failure? failure) error,
  }) {
    return error(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
  }) {
    return error?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Announcement> announcements)? loaded,
    TResult Function(Failure? failure)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AnnouncementsStateInitial value) initial,
    required TResult Function(_AnnouncementsStateLoading value) loading,
    required TResult Function(_AnnouncementsStateLoaded value) loaded,
    required TResult Function(_AnnouncementsStateError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AnnouncementsStateInitial value)? initial,
    TResult Function(_AnnouncementsStateLoading value)? loading,
    TResult Function(_AnnouncementsStateLoaded value)? loaded,
    TResult Function(_AnnouncementsStateError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _AnnouncementsStateError implements AnnouncementsState {
  const factory _AnnouncementsStateError({final Failure? failure}) =
      _$_AnnouncementsStateError;

  Failure? get failure;
  @JsonKey(ignore: true)
  _$$_AnnouncementsStateErrorCopyWith<_$_AnnouncementsStateError>
      get copyWith => throw _privateConstructorUsedError;
}
