import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/error/failures.dart';
import 'package:flutter_task/features/announcements/domain/repositories/announcement_repo.dart';
import 'package:injectable/injectable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/models/announcement.dart';

part 'announcements_state.dart';
part 'announcements_cubit.freezed.dart';

@injectable
class AnnouncementsCubit extends Cubit<AnnouncementsState> {
  final AnnouncementRepo _announcementRepo;
  AnnouncementsCubit(this._announcementRepo)
      : super(const AnnouncementsState.initial());

  Future<void> publishAnnouncement(Announcement announcement) async {
    emit(const AnnouncementsState.loading());
    var either = await _announcementRepo.publishAnnouncement(announcement);
    either.fold((failure) => emit(AnnouncementsState.error(failure: failure)),
        (_) => getAnnouncementsById());
  }

  Future<void> getAnnouncements() async {
    emit(const AnnouncementsState.loading());
    var announcementsEither = await _announcementRepo.getAnnouncements();
    announcementsEither.fold(
        (failure) => emit(AnnouncementsState.error(failure: failure)),
        (announcements) =>
            emit(AnnouncementsState.loaded(announcements: announcements)));
  }

  Future<void> getAnnouncementsById() async {
    emit(const AnnouncementsState.loading());
    var announcementsEither = await _announcementRepo.getAnnouncementsById();
    announcementsEither.fold(
        (failure) => emit(AnnouncementsState.error(failure: failure)),
        (announcements) =>
            emit(AnnouncementsState.loaded(announcements: announcements)));
  }
}
