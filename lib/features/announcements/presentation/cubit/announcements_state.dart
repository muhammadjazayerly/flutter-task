part of 'announcements_cubit.dart';

@freezed
class AnnouncementsState with _$AnnouncementsState {
  const factory AnnouncementsState.initial() = _AnnouncementsStateInitial;
  const factory AnnouncementsState.loading() = _AnnouncementsStateLoading;
  const factory AnnouncementsState.loaded(
      {required List<Announcement> announcements}) = _AnnouncementsStateLoaded;
  const factory AnnouncementsState.error({final Failure? failure}) =
      _AnnouncementsStateError;
}
