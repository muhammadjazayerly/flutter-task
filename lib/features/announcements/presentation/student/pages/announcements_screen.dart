import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/config/app_router.gr.dart';
import '../../../../../core/error/failures.dart';
import '../../../../../injection.dart';
import '../../../../auth/presentation/cubit/auth_cubit.dart';
import '../../cubit/announcements_cubit.dart';
import '../../widgets/announcements_list_widget.dart';

class AnnouncementsScreen extends StatelessWidget {
  const AnnouncementsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<AnnouncementsCubit>(),
      child: const AnnounementsWidget(),
    );
  }
}

class AnnounementsWidget extends StatefulWidget {
  const AnnounementsWidget({Key? key}) : super(key: key);

  @override
  State<AnnounementsWidget> createState() => _AnnounementsWidgetState();
}

class _AnnounementsWidgetState extends State<AnnounementsWidget> {
  @override
  void initState() {
    context.read<AnnouncementsCubit>().getAnnouncements();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        state.maybeWhen(
            unauthenticated: () {
              context.router.replace(const LoginScreen());
            },
            orElse: () {});
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Announcements'),
          actions: [
            IconButton(
              onPressed: () {
                context.read<AuthCubit>().logout();
              },
              tooltip: 'logout',
              icon: const Icon(Icons.logout_rounded),
            ),
          ],
        ),
        body: BlocBuilder<AnnouncementsCubit, AnnouncementsState>(
          builder: (context, state) {
            return state.when(
              initial: () => const Center(
                child: Text('No Announcements have been published yet'),
              ),
              loading: () => const Center(
                child: CircularProgressIndicator.adaptive(),
              ),
              loaded: (announcements) => (announcements.isEmpty)
                  ? const Center(
                      child: Text('No Announcements have been published yet'))
                  : RefreshIndicator(
                      onRefresh: () async {
                        context.read<AnnouncementsCubit>().getAnnouncements();
                      },
                      child: AnnouncementsListWidget(
                        announcements: announcements,
                        showPublisher: true,
                      ),
                    ),
              error: (failure) {
                if (failure is NetworkFailure) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('no internet, please check your connection'),
                        const SizedBox(height: 4),
                        TextButton(
                          onPressed: () {
                            context
                                .read<AnnouncementsCubit>()
                                .getAnnouncements();
                          },
                          child: const Text('retry'),
                        ),
                      ],
                    ),
                  );
                }
                return Center(
                  child: Column(
                    children: [
                      const Text('an error occured'),
                      const SizedBox(height: 4),
                      TextButton(
                        onPressed: () {
                          context.read<AnnouncementsCubit>().getAnnouncements();
                        },
                        child: const Text('retry'),
                      ),
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
