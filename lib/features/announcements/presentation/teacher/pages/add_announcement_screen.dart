import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/functions.dart';
import 'package:flutter_task/core/widgets/custom_button.dart';
import 'package:flutter_task/core/widgets/custom_text_field.dart';
import 'package:flutter_task/features/announcements/data/models/announcement.dart';
import 'package:flutter_task/features/announcements/presentation/cubit/announcements_cubit.dart';
import 'package:flutter_task/injection.dart';
import 'package:uuid/uuid.dart';

import '../../../../../core/error/failures.dart';

class AddAnnouncementScreen extends StatelessWidget {
  const AddAnnouncementScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: getIt<AnnouncementsCubit>(),
      child: Builder(builder: (context) {
        return const AddAnnouncementWidget();
      }),
    );
  }
}

class AddAnnouncementWidget extends StatefulWidget {
  const AddAnnouncementWidget({Key? key}) : super(key: key);

  @override
  State<AddAnnouncementWidget> createState() => _AddAnnouncementWidgetState();
}

class _AddAnnouncementWidgetState extends State<AddAnnouncementWidget> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();

  final _descriptionController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocListener<AnnouncementsCubit, AnnouncementsState>(
      listener: (context, state) {
        state.maybeWhen(
            loaded: (announcements) {
              context.router.pop();
            },
            error: (failure) {
              if (failure is NetworkFailure) {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('no internet, please check your connection'),
                    behavior: SnackBarBehavior.floating,
                  ),
                );
              }
            },
            orElse: () {});
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Publish Announcement'),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Center(
              child: Column(
                children: [
                  const SizedBox(height: 40),
                  CustomTextField(
                    hint: 'Title',
                    controller: _titleController,
                    validator: nameValidator,
                  ),
                  const SizedBox(height: 30),
                  CustomTextField(
                    hint: 'Description',
                    controller: _descriptionController,
                    validator: nameValidator,
                    textInputType: TextInputType.multiline,
                    maxLines: 4,
                  ),
                  const SizedBox(height: 40),
                  BlocBuilder<AnnouncementsCubit, AnnouncementsState>(
                    builder: (context, state) {
                      return state.maybeWhen(
                        loading: () => const Center(
                            child: CircularProgressIndicator.adaptive()),
                        orElse: () => CustomButton(
                          onClick: () {
                            if (_formKey.currentState!.validate()) {
                              context
                                  .read<AnnouncementsCubit>()
                                  .publishAnnouncement(
                                    Announcement(
                                      id: const Uuid().v1(),
                                      publisherName: 'publisher',
                                      publisherId: '',
                                      title: _titleController.text,
                                      description: _descriptionController.text,
                                    ),
                                  );
                            }
                          },
                          text: 'Publish',
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
