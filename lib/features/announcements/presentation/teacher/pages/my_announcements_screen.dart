import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_task/core/config/app_router.gr.dart';
import 'package:flutter_task/features/announcements/presentation/cubit/announcements_cubit.dart';
import 'package:flutter_task/features/auth/presentation/cubit/auth_cubit.dart';
import 'package:flutter_task/injection.dart';

import '../../../../../core/error/failures.dart';
import '../../widgets/announcements_list_widget.dart';

class MyAnnouncementsScreen extends StatelessWidget {
  const MyAnnouncementsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<AnnouncementsCubit>(),
      child: const MyAnnounementsWidget(),
    );
  }
}

class MyAnnounementsWidget extends StatefulWidget {
  const MyAnnounementsWidget({Key? key}) : super(key: key);

  @override
  State<MyAnnounementsWidget> createState() => _MyAnnounementsWidgetState();
}

class _MyAnnounementsWidgetState extends State<MyAnnounementsWidget> {
  @override
  void initState() {
    context.read<AnnouncementsCubit>().getAnnouncementsById();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        state.maybeWhen(
            unauthenticated: () {
              context.router.replace(const LoginScreen());
            },
            orElse: () {});
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Announcements'),
          actions: [
            IconButton(
              onPressed: () {
                context.read<AuthCubit>().logout();
              },
              tooltip: 'logout',
              icon: const Icon(Icons.logout_rounded),
            ),
          ],
        ),
        body: BlocBuilder<AnnouncementsCubit, AnnouncementsState>(
          builder: (context, state) {
            return state.when(
              initial: () => const Center(
                child: Text('No Announcements have been published yet'),
              ),
              loading: () => const Center(
                child: CircularProgressIndicator.adaptive(),
              ),
              loaded: (announcements) => (announcements.isEmpty)
                  ? const Center(
                      child: Text('No Announcements have been published yet'))
                  : AnnouncementsListWidget(announcements: announcements),
              error: (failure) {
                if (failure is NetworkFailure) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('no internet, please check your connection'),
                        const SizedBox(height: 4),
                        TextButton(
                          onPressed: () {
                            context
                                .read<AnnouncementsCubit>()
                                .getAnnouncementsById();
                          },
                          child: const Text('retry'),
                        ),
                      ],
                    ),
                  );
                }
                return Center(
                  child: Column(
                    children: [
                      const Text('an error occured'),
                      const SizedBox(height: 4),
                      TextButton(
                        onPressed: () {
                          context
                              .read<AnnouncementsCubit>()
                              .getAnnouncementsById();
                        },
                        child: const Text('retry'),
                      ),
                    ],
                  ),
                );
              },
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            context.router.push(const AddAnnouncementScreen());
          },
          child: const Icon(Icons.add_comment_rounded),
        ),
      ),
    );
  }
}
