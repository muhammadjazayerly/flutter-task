import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_task/features/auth/data/datasources/auth_remote_ds.dart';
import 'package:flutter_task/features/auth/data/models/user.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/announcement.dart';

abstract class AnnouncementRemoteDS {
  Future<void> publishAnnouncement(Announcement announcement);
  Future<List<Announcement>> getAnnouncements();
  Future<List<Announcement>> getAnnouncementsById();
}

@LazySingleton(as: AnnouncementRemoteDS)
class AnnouncementRemoteDSImpl implements AnnouncementRemoteDS {
  final FirebaseFirestore _firestore;
  final SharedPreferences _preferences;

  AnnouncementRemoteDSImpl(this._firestore, this._preferences);
  @override
  Future<List<Announcement>> getAnnouncements() async {
    var res = await _firestore.collection('announcements').get();
    var announcements =
        res.docs.map((e) => Announcement.fromMap(e.data())).toList();
    return announcements;
  }

  @override
  Future<List<Announcement>> getAnnouncementsById() async {
    var id = User.fromJson(_preferences.get(USER_KEY) as String).id;
    var res = await _firestore
        .collection('announcements')
        .where('publisherId', isEqualTo: id)
        .get();
    var announcements =
        res.docs.map((e) => Announcement.fromMap(e.data())).toList();
    return announcements;
  }

  @override
  Future<void> publishAnnouncement(Announcement announcement) async {
    var user = User.fromJson(_preferences.get(USER_KEY) as String);

    await _firestore.collection('announcements').add(announcement
        .copyWith(publisherName: user.name, publisherId: user.id)
        .toMap());
  }
}
