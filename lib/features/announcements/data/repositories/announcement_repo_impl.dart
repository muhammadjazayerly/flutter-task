import 'package:flutter_task/core/error/exceptions.dart';
import 'package:flutter_task/core/request.dart';
import 'package:flutter_task/features/announcements/data/datasources/announcement_remote_ds.dart';
import 'package:flutter_task/features/announcements/data/models/announcement.dart';
import 'package:flutter_task/core/error/failures.dart';
import 'package:flutter_task/features/announcements/domain/repositories/announcement_repo.dart';
import 'package:fpdart/fpdart.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: AnnouncementRepo)
class AnnouncementRepoImpl implements AnnouncementRepo {
  final AnnouncementRemoteDS _remoteDS;

  AnnouncementRepoImpl(this._remoteDS);
  @override
  Future<Either<Failure, List<Announcement>>> getAnnouncements() async {
    try {
      return await sendRequest<Either<Failure, List<Announcement>>>(() async {
        var announcements = await _remoteDS.getAnnouncements();
        return Right<Failure, List<Announcement>>(announcements);
      });
    } on NetworkException {
      return Left<Failure, List<Announcement>>(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<Announcement>>> getAnnouncementsById() async {
    try {
      return await sendRequest<Either<Failure, List<Announcement>>>(() async {
        var announcements = await _remoteDS.getAnnouncementsById();
        return Right<Failure, List<Announcement>>(announcements);
      });
    } on NetworkException {
      return Left<Failure, List<Announcement>>(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> publishAnnouncement(
      Announcement announcement) async {
    try {
      return await sendRequest<Either<Failure, void>>(() async {
        await _remoteDS.publishAnnouncement(announcement);
        return const Right<Failure, void>(null);
      });
    } on NetworkException {
      return Left<Failure, void>(NetworkFailure());
    }
  }
}
