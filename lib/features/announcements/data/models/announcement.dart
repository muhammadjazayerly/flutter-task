import 'dart:convert';

class Announcement {
  final String id;
  final String publisherName;
  final String publisherId;
  final String title;
  final String description;
  Announcement({
    required this.id,
    required this.publisherName,
    required this.publisherId,
    required this.title,
    required this.description,
  });

  Announcement copyWith({
    String? id,
    String? publisherName,
    String? publisherId,
    String? title,
    String? description,
  }) {
    return Announcement(
      id: id ?? this.id,
      publisherName: publisherName ?? this.publisherName,
      publisherId: publisherId ?? this.publisherId,
      title: title ?? this.title,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'publisherName': publisherName,
      'publisherId': publisherId,
      'title': title,
      'description': description,
    };
  }

  factory Announcement.fromMap(Map<String, dynamic> map) {
    return Announcement(
      id: map['id'] ?? '',
      publisherName: map['publisherName'] ?? '',
      publisherId: map['publisherId'] ?? '',
      title: map['title'] ?? '',
      description: map['description'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Announcement.fromJson(String source) =>
      Announcement.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Announcement(id: $id, publisherName: $publisherName, publisherId: $publisherId, title: $title, description: $description)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Announcement &&
        other.id == id &&
        other.publisherName == publisherName &&
        other.publisherId == publisherId &&
        other.title == title &&
        other.description == description;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        publisherName.hashCode ^
        publisherId.hashCode ^
        title.hashCode ^
        description.hashCode;
  }
}
