// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:cloud_firestore/cloud_firestore.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i4;

import 'core/app_module.dart' as _i13;
import 'features/announcements/data/datasources/announcement_remote_ds.dart'
    as _i5;
import 'features/announcements/data/repositories/announcement_repo_impl.dart'
    as _i7;
import 'features/announcements/domain/repositories/announcement_repo.dart'
    as _i6;
import 'features/announcements/presentation/cubit/announcements_cubit.dart'
    as _i8;
import 'features/auth/data/datasources/auth_remote_ds.dart' as _i9;
import 'features/auth/data/repositories/auth_repo_impl.dart' as _i11;
import 'features/auth/domain/repositories/auth_repo.dart' as _i10;
import 'features/auth/presentation/cubit/auth_cubit.dart'
    as _i12; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final appModule = _$AppModule();
  gh.singleton<_i3.FirebaseFirestore>(appModule.firestore);
  await gh.factoryAsync<_i4.SharedPreferences>(
    () => appModule.sharedpreferences,
    preResolve: true,
  );
  gh.lazySingleton<_i5.AnnouncementRemoteDS>(() => _i5.AnnouncementRemoteDSImpl(
        get<_i3.FirebaseFirestore>(),
        get<_i4.SharedPreferences>(),
      ));
  gh.lazySingleton<_i6.AnnouncementRepo>(
      () => _i7.AnnouncementRepoImpl(get<_i5.AnnouncementRemoteDS>()));
  gh.factory<_i8.AnnouncementsCubit>(
      () => _i8.AnnouncementsCubit(get<_i6.AnnouncementRepo>()));
  gh.lazySingleton<_i9.AuthRemoteDS>(() => _i9.AuthRemoteDSImpl(
        get<_i4.SharedPreferences>(),
        get<_i3.FirebaseFirestore>(),
      ));
  gh.lazySingleton<_i10.AuthRepo>(
      () => _i11.AuthRepoImpl(get<_i9.AuthRemoteDS>()));
  gh.factory<_i12.AuthCubit>(() => _i12.AuthCubit(get<_i10.AuthRepo>()));
  return get;
}

class _$AppModule extends _i13.AppModule {}
