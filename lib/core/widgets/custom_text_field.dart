import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomTextField extends StatefulWidget {
  final String hint;
  final TextEditingController controller;
  final IconData? icon;
  bool obscureText;
  final TextInputType? textInputType;
  final String? Function(String?)? validator;
  final String? errorText;
  final bool toggleShowHideText;
  final double width;
  final int? maxLines;

  CustomTextField({
    Key? key,
    required this.hint,
    required this.controller,
    this.icon,
    this.obscureText = false,
    this.textInputType,
    this.validator,
    this.errorText,
    this.toggleShowHideText = false,
    this.width = 300,
    this.maxLines,
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      // height: 50,
      child: TextFormField(
        controller: widget.controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(16),
            ),
          ),
          hintText: widget.hint,
          prefixIcon: (widget.icon == null) ? null : Icon(widget.icon),
          suffixIcon: FocusScope(
            autofocus: false,
            canRequestFocus: false,
            child: Visibility(
              visible: widget.toggleShowHideText,
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: IconButton(
                  onPressed: () {
                    setState(() {
                      widget.obscureText = !widget.obscureText;
                    });
                  },
                  icon: FaIcon(
                    (widget.obscureText)
                        ? FontAwesomeIcons.eye
                        : FontAwesomeIcons.eyeSlash,
                    size: 22,
                  ),
                ),
              ),
            ),
          ),
        ),
        obscureText: widget.obscureText,
        textInputAction: (widget.textInputType == TextInputType.multiline)
            ? TextInputAction.newline
            : TextInputAction.next,
        maxLines: (widget.obscureText) ? 1 : widget.maxLines,
        keyboardType: widget.textInputType,
        validator: widget.validator,
        inputFormatters: (widget.textInputType == TextInputType.number)
            ? [FilteringTextInputFormatter.digitsOnly]
            : null,
      ),
    );
  }
}
