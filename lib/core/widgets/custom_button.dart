import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onClick;
  final String text;
  final Color? color;
  final Color? textColor;
  final IconData? icon;

  const CustomButton({
    Key? key,
    required this.onClick,
    required this.text,
    this.color,
    this.textColor,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: 50,
      child: ElevatedButton(
        onPressed: onClick,
        style: ElevatedButton.styleFrom(
          primary: color ?? Theme.of(context).colorScheme.primary,
          shadowColor: color ?? Theme.of(context).colorScheme.shadow,
          onPrimary: textColor ?? Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
              side: BorderSide(
                width: 1,
                color: textColor ?? Colors.transparent,
              )),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: icon != null,
              child: FaIcon(
                icon,
                size: 18,
              ),
            ),
            Visibility(
              visible: icon != null,
              child: const SizedBox(width: 20),
            ),
            Text(
              text,
              style: const TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
