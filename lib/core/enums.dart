enum UserType {
  teacher,
  student;

  String toMap() => name;
  static UserType fromMap(String json) => values.byName(json);
}
