import 'dart:io';

import 'error/exceptions.dart';
import 'internet_info.dart';

Future<T> sendRequest<T>(Function onlineCall, {Function? offlineCall}) async {
  if (await InternetInfo.isConnected()) {
    try {
      return await onlineCall();
    } on SocketException {
      throw NetworkException();
    }
  } else {
    if (offlineCall != null) {
      return await offlineCall();
    } else {
      throw NetworkException();
    }
  }
}
