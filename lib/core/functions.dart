String? emailValidator(String? text) {
  RegExp emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  if (text!.isEmpty) {
    return 'Required field';
  } else if (!emailRegExp.hasMatch(text)) {
    return 'Invalid Email';
  }
  return null;
}

String? passwordValidator(String? text) {
  if (text!.isEmpty) {
    return 'Required field';
  } else if (text.length < 6) {
    return 'Should be at least 6 characters';
  }
  return null;
}

String? nameValidator(String? text) {
  if (text!.trim().isEmpty) return 'Required field';
  return null;
}
