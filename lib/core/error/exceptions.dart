class NetworkException implements Exception {}

class CacheException implements Exception {}

// Auth Exceptions
class EmailAlreadyExsitsException implements Exception {}

class EmailOrPasswordIsWrongException implements Exception {}
