abstract class Failure {}

class NetworkFailure extends Failure {}

class CacheFailure extends Failure {}

// Auth Failures
class EmailAlreadyExsitsFailure extends Failure {}

class EmailOrPasswordIsWrongFailure extends Failure {}

class FirebaseFailure extends Failure {
  final String? message;

  FirebaseFailure({this.message});
}
