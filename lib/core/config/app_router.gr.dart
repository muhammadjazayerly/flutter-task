// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;

import '../../features/announcements/presentation/student/pages/announcements_screen.dart'
    as _i4;
import '../../features/announcements/presentation/teacher/pages/add_announcement_screen.dart'
    as _i6;
import '../../features/announcements/presentation/teacher/pages/my_announcements_screen.dart'
    as _i5;
import '../../features/auth/presentation/pages/login_screen.dart' as _i2;
import '../../features/auth/presentation/pages/register_screen.dart' as _i3;
import '../widgets/splash_screen.dart' as _i1;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    SplashScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.SplashScreen(),
      );
    },
    LoginScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i2.LoginScreen(),
      );
    },
    RegisterScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i3.RegisterScreen(),
      );
    },
    AnnouncementsScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i4.AnnouncementsScreen(),
      );
    },
    MyAnnouncementsScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i5.MyAnnouncementsScreen(),
        maintainState: false,
      );
    },
    AddAnnouncementScreen.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i6.AddAnnouncementScreen(),
      );
    },
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig(
          SplashScreen.name,
          path: '/',
        ),
        _i7.RouteConfig(
          LoginScreen.name,
          path: '/login-screen',
        ),
        _i7.RouteConfig(
          RegisterScreen.name,
          path: '/register-screen',
        ),
        _i7.RouteConfig(
          AnnouncementsScreen.name,
          path: '/announcements-screen',
        ),
        _i7.RouteConfig(
          MyAnnouncementsScreen.name,
          path: '/my-announcements-screen',
        ),
        _i7.RouteConfig(
          AddAnnouncementScreen.name,
          path: '/add-announcement-screen',
        ),
      ];
}

/// generated route for
/// [_i1.SplashScreen]
class SplashScreen extends _i7.PageRouteInfo<void> {
  const SplashScreen()
      : super(
          SplashScreen.name,
          path: '/',
        );

  static const String name = 'SplashScreen';
}

/// generated route for
/// [_i2.LoginScreen]
class LoginScreen extends _i7.PageRouteInfo<void> {
  const LoginScreen()
      : super(
          LoginScreen.name,
          path: '/login-screen',
        );

  static const String name = 'LoginScreen';
}

/// generated route for
/// [_i3.RegisterScreen]
class RegisterScreen extends _i7.PageRouteInfo<void> {
  const RegisterScreen()
      : super(
          RegisterScreen.name,
          path: '/register-screen',
        );

  static const String name = 'RegisterScreen';
}

/// generated route for
/// [_i4.AnnouncementsScreen]
class AnnouncementsScreen extends _i7.PageRouteInfo<void> {
  const AnnouncementsScreen()
      : super(
          AnnouncementsScreen.name,
          path: '/announcements-screen',
        );

  static const String name = 'AnnouncementsScreen';
}

/// generated route for
/// [_i5.MyAnnouncementsScreen]
class MyAnnouncementsScreen extends _i7.PageRouteInfo<void> {
  const MyAnnouncementsScreen()
      : super(
          MyAnnouncementsScreen.name,
          path: '/my-announcements-screen',
        );

  static const String name = 'MyAnnouncementsScreen';
}

/// generated route for
/// [_i6.AddAnnouncementScreen]
class AddAnnouncementScreen extends _i7.PageRouteInfo<void> {
  const AddAnnouncementScreen()
      : super(
          AddAnnouncementScreen.name,
          path: '/add-announcement-screen',
        );

  static const String name = 'AddAnnouncementScreen';
}
