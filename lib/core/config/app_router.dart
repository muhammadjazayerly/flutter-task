import 'package:auto_route/annotations.dart';
import 'package:flutter_task/core/widgets/splash_screen.dart';
import 'package:flutter_task/features/announcements/presentation/student/pages/announcements_screen.dart';
import 'package:flutter_task/features/announcements/presentation/teacher/pages/add_announcement_screen.dart';
import 'package:flutter_task/features/announcements/presentation/teacher/pages/my_announcements_screen.dart';
import 'package:flutter_task/features/auth/presentation/pages/login_screen.dart';
import 'package:flutter_task/features/auth/presentation/pages/register_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: SplashScreen, initial: true),
    AutoRoute(page: LoginScreen),
    AutoRoute(page: RegisterScreen),
    AutoRoute(page: AnnouncementsScreen),
    AutoRoute(page: MyAnnouncementsScreen, maintainState: false),
    AutoRoute(page: AddAnnouncementScreen),
  ],
)
class $AppRouter {}
