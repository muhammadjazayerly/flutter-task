import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
abstract class AppModule {
  @Singleton()
  FirebaseFirestore get firestore => FirebaseFirestore.instance;

  @preResolve
  Future<SharedPreferences> get sharedpreferences async =>
      await SharedPreferences.getInstance();
}
